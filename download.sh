# Bash script to download playlists and store them in mp3 format


Playlists=(

		"https://www.youtube.com/playlist?list=PL1H9u5XRF8do1K_YGoAiwzlw1GXpiACOD"		# Anime OSTs
		"https://www.youtube.com/playlist?list=PL1H9u5XRF8dpujFFYeggn3B-q9L0ULe0U"		# Bleach OSTs
		"https://www.youtube.com/playlist?list=PL1H9u5XRF8drkanmExnJAWXQxsT3alTo4"		# EDM
		"https://www.youtube.com/playlist?list=PL1H9u5XRF8domnrbyPYc-kz6jc4CyxpQq"		# Hindi Songs
		"https://www.youtube.com/playlist?list=PL1H9u5XRF8dpykUcTGDtIgH_J5B2q6x6v"		# Interersting songs
		"https://www.youtube.com/playlist?list=PL1H9u5XRF8doIGQw2mEzFOWo_jMJtErSd"		# KPOP 
		"https://www.youtube.com/playlist?list=PL1H9u5XRF8dpBFxW184UKZPJaunVTC2UQ"		# League of Legends MVs
		"https://www.youtube.com/playlist?list=PL1H9u5XRF8do9YQNY77cjbm5ubXWiUtVV"		# Nightcore
		"https://www.youtube.com/playlist?list=PL1H9u5XRF8dp2tJyVTAaW9vP53qSe2-M7"		# Songs

	)


for playlist in "${Playlists[@]}"; do
	youtube-dl -r 100K --download-archive ~/Music/Youtube/archive.txt --ignore-errors --extract-audio --audio-format mp3 -o '~/Music/Youtube/%(playlist)s/%(playlist_index)s - %(title)s.%(ext)s' "$playlist"
done
